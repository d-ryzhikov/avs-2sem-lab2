format MZ
entry main:start

include 'selectors.inc'
include 'macros.inc'

segment main use16
set_descriptor:
;eax=segment address
;ds:bx=descriptor address
;cl=access rights byte
;edx=limit

    push cx             ;save cx value for further use
    mov cx,ax           ;cx=2 lower bytes of address
    shl ecx,16          ;move cx to upper bytes of ecx
    mov cx,dx           ;cx=0..15 bits of limit
    mov [bx],ecx        ;save 4 lower bytes of descriptor
    shr eax,16          ;ax=2 higher bytes of address
    mov ch,ah           ;ch=4th byte of address
    shr edx,16          ;dl=0000+16..19 bits of limit
    mov cl,dl
    shl ecx,16          ;move cx to upper bytes of ecx
    mov cl,al           ;cl=3rd byte of address
    pop ax              ;al=access rights byte
    mov ch,al
    mov [bx+4],ecx      ;save 4 higher bytes of descriptor
    add bx,8            ;point at next descriptor
    ret

delay:
    push bx
    push cx

    mov bx,1500
outer_loop:
    mov cx,0FFFFh
    inner_loop:
        dec cx
        nop
        jnz inner_loop
    dec bx
    nop
    jnz outer_loop

    pop bx
    pop cx
    ret

start:
    mov ax,main
    mov ds,ax

;save code segment and offset value to restore from protected mode
    mov [rm_segment],cs
    lea ax,[back_to_rm]
    mov [rm_offset],ax

;save register values
    mov [rm_ss],ss
    mov [rm_ds],ds
    mov [rm_es],es
    mov [rm_fs],fs
    mov [rm_gs],gs

    xor eax,eax
    mov ax,cs
    shl eax,4           ;eax=physical address of main segment
    mov [segment_address],eax
    lea bx,[GDT+8]      ;bx=address of code descriptor in GDT
    mov edx,1024        ;set limit to 1024 bytes
    mov cl,10011000b    ;P=1, DPL=00b, S=1, Type=100b, A=0
    call set_descriptor ;set code segment descriptor

    mov eax,[segment_address]
    xor edx,edx
    lea dx,[stack_start]
    add eax,edx         ;eax=physical address of stack beginning
    mov edx,1024        ;set limit to 1024 bytes
    mov cl,10010110b    ;P=1, DPL=00b, S=1, Type=011b, A=0
    call set_descriptor ;set stack segment descriptor

    mov eax,[segment_address]
    mov edx,0FFFFh
    mov cl,10010010b    ;P=1, DPL=00b, S=1, Type=001b, A=0
    call set_descriptor ;set data descriptor

    mov eax,0b8000h     ;video memory address
    mov edx,4000        ;video memory segment limit
    mov cl,10110010b    ;P=1, DPL=01b, S=1, Type=001b, A=0
    call set_descriptor ;set video memory segment

    mov eax,[segment_address]
    mov edx,0FFFFh
    mov cl,10011010b    ;P=1, DPL=00b, S=1, Type=101b, A=0
    call set_descriptor ;set r-mode code descriptor

    mov eax,[segment_address]
    mov edx,0FFFFh
    mov cl,10010010b    ;P=1, DPL=00b, S=1, Type=001b, A=0
    call set_descriptor ;set r-mode data descriptor

;set LDT descriptors
    mov eax,[segment_address]
    xor edx,edx
    lea dx,[LDT_1]
    add eax,edx
    push eax
    mov edx,31
    mov cl,10100010b    ;P=1, DPL=01b
    call set_descriptor ;1st task's LDT

    pop eax
    add eax,32          ;eax=2nd task's LDT physical address
    push eax
    mov edx,31
    mov cl,11000010b    ;P=1, DPL=10b
    call set_descriptor ;2nd task's LDT

    pop eax
    add eax,32          ;eax=3rd task's LDT physical address
    mov edx,31
    mov cl,11100010b    ;P=1, DPL=11b
    call set_descriptor ;3rd task's LDT

;set TSS descriptors
    mov eax,[segment_address]
    xor edx,edx
    lea dx,[TSS_main]
    add eax,edx         ;eax=main task's TSS physical address
    push eax
    mov edx,103         ;TSS size - 1
    mov cl,10001001b    ;P=1, DPL=00b, S=0, B=0
    call set_descriptor ;main task TSS

    pop eax
    add eax,68h         ;eax=1st task's TSS physical address
    push eax
    mov edx,103
    mov cl,10101001b    ;P=1, DPL=01b, S=0, B=0
    call set_descriptor ;1st task TSS

    pop eax
    add eax,68h         ;eax=2nd task's TSS physical address
    push eax
    mov edx,103
    mov cl,11001001b    ;P=1, DPL=10b, S=0, B=0
    call set_descriptor ;2nd task TSS

    pop eax
    add eax,68h         ;eax=3rd task's TSS physical address
    mov edx,103
    mov cl,11101001b    ;P=1, DPL=11b, S=0, B=0
    call set_descriptor ;3rd task TSS

;LDT_1 DPL=01b
init_LDT LDT_1,task_1,task_1_stack_start,task_1_data,task_1_data_end,00100000b

;LDT_2 DPL=10b
init_LDT LDT_2,task_2,task_2_stack_start,task_2_data,task_2_data_end,01000000b

;LDT_3 DPL=11b
init_LDT LDT_3,task_3,task_3_stack_start,task_3_data,task_3_data_end,01100000b

int 1
init_TSS TSS_1,LDT_1_CODE_SELECTOR,LDT_1_STACK_SELECTOR,LDT_1_DATA_SELECTOR,LDT_1_SELECTOR,task_1_colors-task_1_data,74

init_TSS TSS_2,LDT_2_CODE_SELECTOR,LDT_2_STACK_SELECTOR,LDT_2_DATA_SELECTOR,LDT_2_SELECTOR,task_2_colors-task_2_data,234

init_TSS TSS_3,LDT_3_CODE_SELECTOR,LDT_3_STACK_SELECTOR,LDT_3_DATA_SELECTOR,LDT_3_SELECTOR,task_3_colors-task_3_data,394

;set GDTR image
    lea cx,[GDT_end]
    lea dx,[GDT]
    sub cx,dx
    mov [GDT_limit],cx  ;limit of GDT
    mov eax,[segment_address]
    xor edx,edx
    lea dx,[GDT]
    add eax,edx
    mov [GDT_address],eax

    clear_screen

    cli
    lgdt fword [GDTR]

;store stack pointer just before switching to protected mode
    mov [rm_sp],sp

;enter protected mode
    mov eax,cr0
    or al,1             ;set PE flag to 1
    mov cr0,eax

    db 0eah             ;far jump opcode
    dw start_pm
    dw CODE_SELECTOR

start_pm:
;initialize segment selectors
    mov ax,STACK_SELECTOR
    mov ss,ax
    mov sp,0

    mov ax,DATA_SELECTOR
    mov ds,ax

    mov ax,VIDEOMEM_SELECTOR
    mov es,ax

    mov ax,TSS_MAIN_SELECTOR
    ltr ax

    lea bx,[GDT+8*10]           ;address of main task TSS descriptor
    and byte [bx+5],11111101b   ;clear main task busy flag

;enter main task
    db 0eah
    dw 0
    dw TSS_MAIN_SELECTOR

    mov cx,10

main_cycle:

    db 9ah
    dw 0
    dw TSS_1_SELECTOR

    call delay

    db 9ah
    dw 0
    dw TSS_2_SELECTOR

    call delay

    db 9ah
    dw 0
    dw TSS_3_SELECTOR

    call delay

    dec cx
    jnz main_cycle

prepare_for_rm:
    mov ax,RM_DATA_SELECTOR
    mov ss,ax
    mov ds,ax
    mov es,ax
    mov fs,ax
    mov gs,ax

    mov eax,cr0
    and al,11111110b    ;set PE flag to 0
    mov cr0,eax

    db 0eah             ;far jump opcode
    rm_offset dw ?      ;will be filled before
    rm_segment dw ?     ;entering protected mode

back_to_rm:
;restore registers' state
    mov ss,[rm_ss]
    mov ds,[rm_ds]
    mov es,[rm_es]
    mov fs,[rm_fs]
    mov gs,[rm_gs]

;restore stack pointer just before enabling interrupts
    mov sp,[rm_sp]
    sti

    clear_screen

;exit
    mov ax,4c00h
    int 21h

task_1:
    task_body
task_2:
    task_body
task_3:
    task_body
;--------------------------------------------------------------------
segment_address dd ?;contains physical address of main segment

;registers' state before entering protected mode
rm_ss dw ?
rm_sp dw ?
rm_ds dw ?
rm_es dw ?
rm_fs dw ?
rm_gs dw ?

;--------------------------------------------------------------------
;GDTR register image
GDTR:
    GDT_limit dw ?
    GDT_address dd ?

align 8
GDT:
    db 8 dup(?);null descriptor
    db 8 dup(?);code segment descriptor
    db 8 dup(?);stack segment descriptor
    db 8 dup(?);data segment descriptor
    db 8 dup(?);video memory segment descriptor
    db 8 dup(?);r-mode code segment descriptor
    db 8 dup(?);r-mode data segment descriptor

    db 8 dup(?);1st task LDT
    db 8 dup(?);2nd task LDT
    db 8 dup(?);3rd task LDT

    db 8 dup(?);main task TSS
    db 8 dup(?);1st task TSS
    db 8 dup(?);2nd task TSS
    db 8 dup(?);3rd task TSS
GDT_end:

LDT_1:
    db 8 dup(?);null descriptor
    db 8 dup(?);code segment descriptor
    db 8 dup(?);stack segment descriptor
    db 8 dup(?);data segment descriptor
LDT_2:
    db 8 dup(?);null descriptor
    db 8 dup(?);code segment descriptor
    db 8 dup(?);stack segment descriptor
    db 8 dup(?);data segment descriptor
LDT_3:
    db 8 dup(?);null descriptor
    db 8 dup(?);code segment descriptor
    db 8 dup(?);stack segment descriptor
    db 8 dup(?);data segment descriptor

TSS_main:
    db 68h dup(0)
TSS_1:
    db 68h dup(0)
TSS_2:
    db 68h dup(0)
TSS_3:
    db 68h dup(0)

;--------------------------------------------------------------------
task_1_data:
    db "Task 1",0
    task_1_colors db 00110001b,01010001b,01110001b
task_1_data_end:

task_2_data:
    db "Task 2",0
    task_2_colors db 00010010b,01000010b,01100010b
task_2_data_end:

task_3_data:
    db "Task 3",0
    task_3_colors db 00110101b,01110101b,00000101b
task_3_data_end:

;--------------------------------------------------------------------
;protected mode stack segment
    db 1024 dup(?);place reserved for task 3 stack
task_3_stack_start:
    db 1024 dup(?);place reserved for task 2 stack
task_2_stack_start:
    db 1024 dup(?);place reserved for task 1 stack
task_1_stack_start:
    db 1024 dup(?);place reserved for main task stack
stack_start:
