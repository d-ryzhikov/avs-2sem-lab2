macro init_LDT LDT, code, stack, data, data_end, DPL
{
;DPL is saved in 2 bits: 5th and 6th
    lea bx,[LDT+8]      ;bx=address of code descriptor in LDT
    mov eax,[segment_address]
    xor edx,edx
    lea dx,[code]
    add eax,edx         ;eax=physical address of task code
    mov edx,1024
    mov cl,10011000b    ;P=1, DPL=XXb, S=1, Type=100b, A=0
    or cl,DPL           ;set DPL
    call set_descriptor ;task code selector

    mov eax,[segment_address]
    xor edx,edx
    lea dx,[stack]
    add eax,edx
    mov edx,1024
    mov cl,10010110b    ;P=1, DPL=XXb, S=1, Type=011b, A=0
    or cl,DPL           ;set DPL
    call set_descriptor ;task stack segment

    mov eax,[segment_address]
    xor edx,edx
    xor ecx,ecx
    lea edx,[data_end]
    lea ecx,[data]
    add eax,ecx         ;physical address of task data segment
    sub dx,cx           ;compute limit of task data segment
    mov cl,10010010b    ;P=1, DPL=XXb, S=1, Type=001b, A=0
    or cl,DPL           ;set DPL
    call set_descriptor ;task data segment
}

macro init_TSS TSS,code_selector,stack_selector,data_selector,LDT_selector,array_offset,text_pos
{
    lea bx,[TSS]
    mov word [bx+4Ch],code_selector     ;set task's CS
    mov word [bx+50h],stack_selector    ;set task's SS
    mov word [bx+54h],data_selector     ;set task's DS
    mov eax,cr3
    mov [bx+1Ch],eax            ;set task's CR3
    pushfd
    pop eax
    and ah,11111101b            ;IF=0
    mov [bx+24h],eax            ;set task's EFLAGS
    mov ax,VIDEOMEM_SELECTOR
    mov [bx+48h],ax             ;set task's ES
    mov word [bx+60h],LDT_selector      ;set TSS LDT selector
    mov word [bx+30h],array_offset      ;DX=offset of colors array
    mov word [bx+44h],text_pos          ;DI=text output position
}

macro task_body text_pos
{
    local task_start, after_check, reset_color
task_start:
    cmp cx,2
    ja reset_color
after_check:
    mov bx,dx
    add bx,cx
    mov ah,byte [bx]
    xor bx,bx
    push di
    print_zs
    iret
    inc cx               ;set cx to point to next color
    pop di               ;restore text output position
    jmp task_start
reset_color:
    xor cx,cx
    jmp after_check
}

macro print_zs
{
;ah contains color byte
    local put_char, str_end
    put_char:
        mov al,[bx]
        inc bx
        test al,al
        jz str_end
        mov [es:di],ax
        add di,2
        jmp put_char
    str_end:
}

macro clear_screen
{
    mov ax,3
    int 10h
}
